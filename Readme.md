**W21_FS1030 Individual Project Database**

Only Include Individual Project Database Components

Project Description:

The project is to design an Portfolio website datadase which integrate MySql database to a backend with the UI to understand overall integration and functionality like:

1. Registration of Admin user.
2. Login.
3. View Users Entries.
4. Porfolio (Add, delete and Update).
5. Resume (Add, delete and update).

The Repo contains the following

IFD Diagram
EER Diagrams
Database Schema SQL statements

**Instruction:**
Open the Portfolio_Database.sql in Mysql workbench and execute the SQL scripts to create database and tables and Insert Dummy data.
**OR **
clone Backend repo https://gitlab.com/naeem_ali/fs1030-portfolio-backend.git and follow Backend repo ReadMe file to create Database and other sql queries
